<?php

namespace App\Http\Controllers;

use App\Page;
use App\Project;
use Illuminate\Http\Request;
use App\Textile;

class AdminController extends Controller
{
    public function index()
    {

    }

    public function textile_view_list()
    {
        $textiles = Textile::all();

        return view('pages.admin.textile-list',compact('textiles'));
    }

    public function textile_view_add()
    {
        $projects = Project::all();
        //return view('admin-textile-add');
        return view('pages.admin.textile-add')->with(compact('projects'));
    }

    public function textile_add(Request $request)
    {


        $imageName = null;

        if( $request->hasFile('textile_img_upload')) {
            $file = $request->file('textile_img_upload');
            $ext = strtolower($file->getClientOriginalExtension());
            $imageName = time().'.'.$ext;
            $file->move(public_path('textile_images'), $imageName);

        }
        //dd($imageName);


        //dd($request->textile_img_upload);
        //$request->image->move(public_path('textile_images'), $imageName);


        $textile = new Textile;
        $textile->name = request('textile-name');
        $textile->detail = request('textile-detail');
        $textile->project_id = request('project_id');
        $textile->textile_image = $imageName;
        $textile->save();

        return redirect('/admin/textile');
    }

    public function textile_view_edit($id)
    {
        $textile = Textile::find($id);
        $projects = Project::all();

        return view('pages.admin.textile-edit',compact('textile'))->with(compact('projects'));
    }

    public function textile_edit($id)
    {

        $imageName = null;

        if( request()->hasFile('textile_img_upload')) {
            $file = request()->file('textile_img_upload');
            $ext = strtolower($file->getClientOriginalExtension());
            $imageName = time().'.'.$ext;
            $file->move(public_path('textile_images'), $imageName);

        }
        //dd($imageName);

        $textile = Textile::find($id);
        $textile->name = request('textile-name');
        $textile->detail = request('textile-detail');
        $textile->textile_image = $imageName;
        $textile->save();
        return redirect('/admin/textile');
    }

    public function textile_delete($id)
    {
        $textile = Textile::find($id);
        $textile->delete();
        return redirect('/admin/textile');
    }

    public function about_research_view_edit()
    {
        return view('pages.admin.about-research-edit');
    }

    public function about_research_edit()
    {
        $page = Page::find(1);
        $page->page_detail = request('about-research-detail');
        $page->save();
        return redirect('/admin/textile');
    }
}
