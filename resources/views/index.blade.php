<!DOCTYPE HTML>
<!--
	Landed by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>ผ้าย้อมทองกวาว</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<link href="https://fonts.googleapis.com/css?family=Athiti:500" rel="stylesheet">
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body class="landing">
		<div id="page-wrapper">

			<!-- Header -->
				<header id="header">
					<h1 id="logo"><a href="/">งานวิจัยผ้าย้อมทองกวาว</a></h1>
					<nav id="nav">
						<ul>
							<li><a href="/">หน้าหลัก</a></li>
							<li>
								<a href="#">เนื้อหา</a>
								<ul>
									<li><a href="/about-research">งานวิจัยผ้าย้อมทองกวาว</a></li>
									<li><a href="/about-tongkwaw">ต้นทองกวาว</a></li>
									<li><a href="/about-cmu">มหาวิทยาลัยเชียงใหม่</a></li>
								</ul>
							</li>

							<li><a href="/textile">ผ้าย้อมดอกทองกวาว</a></li>
							
						</ul>
					</nav>
				</header>

			<!-- Banner -->
				<section id="banner" style="margin-bottom: 0.35em;">
					<div class="content">
						<header>
							<h2>ผ้าย้อมทองกวาว</h2>
							<p>นำเสนอเรื่องราวโดย<br />
							คณะวิทยาศาสตร์ มหาวิทยาลัยเชียงใหม่</p>
						</header>
						<span class="image"><img src="images/pic01.png" alt="" /></span>
					</div>
					<a href="#one" class="goto-next scrolly">Next</a>
				</section>

				<section id="one" class="spotlight style3 left">
					<span class="image fit main bottom"><img src="images/pic04.jpg" alt="" /></span>
					<div class="content">
						<header>
							<h2>ผ้าย้อมทองกวาว</h2>
							<p>เอกลักษณ์ของมหาวิทยาลัยเชียงใหม่ที่ถูกสร้างสรรค์ขึ้นในรูปผ้าย้อมสีทองกวาว</p>
						</header>
						<p>ผ้าย้อมทองกวาวเป็นหนึ่งในงานวิจัยของคณะวิทยาศาสตร์ มหาวิยาลัยเชียงใหม่ ถูกคิดคิ้นและสร้างสรรค์โดยนักวิจัยชาว มช. โดยนำดอกทองกวาวซึ่งเป็นต้นไม้ประจำมหาวิทยาลัยเชียงใหม่มาสกัดเป็นสีสันที่ย้อมบนลงตัวผ้าซึ่งให้สีสันที่สวยงาม และให้สีที่มีความเป็นเอกลักษณ์ของดอกทองกวาวและความเป็นมหาวิทยาลัยเชียงใหม่</p>
						<ul class="actions">
							<li><a href="/about-research" class="button">อ่านเพิ่มเติม</a></li>
						</ul>
					</div>
					<a href="#two" class="goto-next scrolly">Next</a>
				</section>


				<section id="two" class="spotlight style2 right">
					<span class="image fit main"><img src="images/pic03.jpg" alt="" /></span>
					<div class="content">
						<header>
							<h2>ต้นทองกวาว</h2>
							<p>ต้นไม้ประจำมหาวิทยาลัยเชียงใหม่</p>
						</header>
						<p>ต้นทองกวาวเป็นต้นไม้ประจำมหาวิทยาลัยเชียงใหม่ ซึ่งในอดีตดอกทองกวาวจะมีอยู่ทั่วไปในบริเวณมหาวิทยาลัยเชียงใหม่ จะออกดอกบานสะพรั่งชูช่อสีส้ม ให้นักศึกษาของมหาวิทยาลัยเชียงใหม่ได้ชื่นชมความงามในทุกฤดูหนาว พื้นสนามและทางเดินจะปูด้วยดอกทองกวาวสีส้มที่ร่วงหล่นเต็มพื้น นักศึกษามหาวิทยาลัยเชียงใหม่ทุกคนจะจดจำดอกทองกวาวได้ดี ปัจจุบันสามารถหาชมได้ บริเวณศาลาอ่างแก้ว หรือ ตามคณะต่างๆ ความงดงามและสีสันของดอกทองกวาวนั้น ได้ช่วยสร้างบรรยากาศแห่งความปลาบปลื้มใจให้แก่อาจารย์ นักศึกษาและบุคลาครในรั่วมหาวิทยาลัยเชียงใหม่ จนทำให้เกิดความผูกพันธ์ตราตรึงไม่จางหายไป จึงถือได้ว่าดอกทองกวาวเป็นดอกไม้ประจำมหาวิทยาลัยเชียงใหม่ย่างแท้จริง</p>
						<ul class="actions">
							<li><a href="/about-tongkwaw" class="button">อ่านเพิ่มเติม</a></li>
						</ul>
					</div>
					<a href="#three" class="goto-next scrolly">Next</a>
				</section>

				<section id="three" class="spotlight style1 bottom">
				<span class="image fit main"><img src="images/pic02.jpg" alt="" /></span>
				<div class="content">
					<div class="container">
						<div class="row">
							<div class="4u 12u$(medium)">
								<header>
									<h2>มหาวิทยาลัยเชียงใหม่</h2>
									<p>อตฺตานํ ทมยนฺติ ปณฺฑิตา บัณฑิตทั้งหลายย่อมฝึกตน</p><p></p>
								</header>
							</div>
							<div class="4u 12u$(medium)">
								<p>มหาวิทยาลัยเชียงใหม่ เป็นมหาวิทยาลัยแห่งแรกในส่วนภูมิภาคจัดตั้งขึ้นตามนโยบายของรัฐ และเจตนารมณ์ของประชาชนในภาคเหนือให้เป็นศูนย์กลางทางวิชาการและวิชาชีพชั้นสูง เพื่ออำนวยประโยชน์แก่ท้องถิ่นและประเทศชาติโดยส่วนรวม</p>
							</div>
							<div class="4u$ 12u$(medium)">
								<p>มหาวิทยาลัยแห่งนี้เป็นแหล่งสะสม ค้นคว้า วิจัย และถ่ายทอดความรู้ ตามหลักแห่งเสรีภาพทางวิชาการ โดยยึดมั่นในสัจธรรมและคุณธรรม เพื่อความเป็นเลิศทางวิชาการ การประยุกต์ เผยแพร่ และการทำนุบำรุงศิลปวัฒนธรรม
									บัณฑิตแห่งมหาวิทยาลัยเชียงใหม่ พึงฝักใฝ่ในการฝึกฝนตน เป็นผู้รู้จริง คิดเป็น ปฏิบัติได้ สามารถครองตน ครองคน ครองงาน ด้วยมโนธรรมและจิตสำนึกต่อสังคม
								</p>
							</div>
						</div>
					</div>
				</div>
				<a href="#four" class="goto-next scrolly">Next</a>
			</section>


			<!-- Four -->
				<section id="four" class="wrapper style1 special fade-up">
					<header class="major">
						<h2>ผ้าย้อมทองกวาว</h2>
						<p>รายชื่อผ้าย้อมดอกทองกวาว</p>
					</header>
					<div class="box alt">
						<div class="row 50% uniform">
							@foreach($textiles as $index=>$textile)
								@if($index < 6)
									<div class="4u  6u(xsmall)" style="text-align:center">{{$textile->name}}<a href="/{{$textile->getProjectNameLink()}}/{{$textile->id}}"><span class="image fit"><img src="@if ($textile->textile_image == null)images/pic04.jpg @else public/textile_images/{{$textile->textile_image}} @endif" alt=""></span></a></div>
								@endif
							@endforeach
						</div>
					</div>
					<footer class="major">
						<ul class="actions">
							<li><a href="/textile" class="button">ดูรายการผ้าเพิ่มเติม</a></li>
						</ul>
					</footer>
				</section>

			<!-- Five -->
				<section id="five" class="wrapper style2 special fade">
				


					<audio autoplay controls="" loop="" preload="">
						<source src="/cmu_march.mp3" type="audio/ogg"></source>
						Your browser does not support the audio element.
					</audio>
					<ul class="copyright" style="list-style:none">
						<li>&copy; Chiang Mai University. All rights reserved.</li>
					</ul>
				</section>


		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>