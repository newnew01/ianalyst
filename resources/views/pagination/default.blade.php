@if ($paginator->lastPage() > 1)
    <ul class="actions">
        <li class="{{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
            <a class="button" href="{{ $paginator->url(1) }}">ก่อนหน้า</a>
        </li>
        @for ($i = 1; $i <= $paginator->lastPage(); $i++)
            <li class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}" >
                @if ($paginator->currentPage() == $i)
                    <a style="background-color: #973ef7" class="button" href="#">{{ $i }}</a>
                @else
                    <a class="button" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                @endif

            </li>
        @endfor
        <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
            <a class="button" href="{{ $paginator->url($paginator->currentPage()+1) }}" >ถัดไป</a>
        </li>
    </ul>
@endif