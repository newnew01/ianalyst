@extends('admin')

@section('title','เกี่ยวกับงานวิจัย')

@section('content')
    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="box-title">เกี่ยวกับงานวิจัย</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form role="form" method="post" action="/admin/page/about-research" enctype="multipart/form-data">

                <!-- textarea -->
                <div class="form-group">
                    <label>ข้อมูล</label>
                    <textarea name="about-research-detail" id="about-research-detail" class="form-control" rows="13">{{\App\Page::find(1)->page_detail}}</textarea>
                </div>


                <button type="submit" class="btn btn-info"><i class="fa fa-edit"></i> แก้ไข</button>
                <a href="/admin/textile"><button type="button" class="btn btn-default">ย้อนกลับ</button></a>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </form>
        </div>
        <!-- /.box-body -->
    </div>
@endsection

@section('js-head')
    <script src="/js/tinymce/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector: "#about-research-detail",
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
            ],
            toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
            toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
            image_advtab: true ,

            external_filemanager_path:"/filemanager/",
            filemanager_title:"Responsive Filemanager" ,
            external_plugins: { "filemanager" : "/filemanager/plugin.min.js"},
            relative_urls: false
        });
    </script>
@endsection